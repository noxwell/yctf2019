#include "car.h"

#include <QtDebug>

#include <cmath>

const double PI = 3.141592653589793;

Car::Car(QObject *parent) : QObject(parent), tcpSocket(new QTcpSocket(this)), out("dmp.txt") {
    assert(out.open(QIODevice::ReadWrite));
    in.setDevice(tcpSocket);
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(logError(QAbstractSocket::SocketError)));
    tcpSocket->connectToHost("2a02:6b8:c12:26ab::41c5:d769:ac0a", 31337);
    int len = in.writeRawData("O\r", 2);
    assert(len == 2);
}

double Car::lat() {
    return lat_;
}

double Car::lon() {
    return lon_;
}

double Car::speed() {
    return speed_;
}

//void Car::setSpeed(double speed) {
//    speed_ = speed;
//}

double Car::angle() {
    return angle_;
}

//void Car::setAngle(double angle) {
//    angle_ = angle;
//}

QList<qreal> Car::angles() {
    return angles_;
}

QList<qreal> Car::distances() {
    return distances_;
}

QString Car::mark() {
    return mark_;
}

double Car::cloudx() {
    return cloudx_;
}

double Car::cloudy() {
    return cloudy_;
}

bool Car::left() {
    return left_;
}

void Car::setLeft(bool left) {
    if (left_ != left) {
        left_ = left;
        updateParams();
        emit leftChanged();
    }
}

bool Car::right() {
    return right_;
}

void Car::setRight(bool right) {
    if (right_ != right) {
        right_ = right;
        updateParams();
        emit rightChanged();
    }
}

bool Car::up() {
    return up_;
}

void Car::setUp(bool up) {
    if (up_ != up) {
        up_ = up;
        updateParams();
        emit upChanged();
    }
}

bool Car::down() {
    return down_;
}

void Car::setDown(bool down) {
    if (down_ != down) {
        down_ = down;
        updateParams();
        emit downChanged();
    }
}

//static char HEX_TBL[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

//char ParseChar(QChar c) {
//    for (char i = 0; i < 16; ++i) {
//        if (HEX_TBL[i] == c) {
//            return i;
//        }
//    }
//    assert(false);
//}

//QByteArray GetBytes(const QString& s) {
//    QByteArray result;
//    assert((s.size() % 2) == 0);
//    for (int i = 0; i < s.size(); i += 2) {
//        char b = (ParseChar(s.at(i)) << 4) | ParseChar(s.at(i + 1));
//        result.append(b);
//    }
//}

double GetDouble(QByteArray b) {
    std::reverse(b.begin(), b.end());
    return *((double*)((void*)b.data()));
}

QByteArray buildFrame(QByteArray frame_id, double value) {
    QByteArray value_bytes;
    value_bytes.resize(8);
    *((double*)((void*)value_bytes.data())) = value;
    std::reverse(value_bytes.begin(), value_bytes.end());
    frame_id.insert(0, 't');
    frame_id.append('8');
    frame_id.append(value_bytes.toHex().toUpper());
    frame_id.append('\r');
    return frame_id;
}

void Car::readData() {
    while (true) {
        int written;

        if (buf_pos_ < 1) {
            written = in.readRawData(buf_, 1);
            out.write(buf_, written);
            buf_pos_ += written;
            if (buf_pos_ < 1) {
                break;
            }
            assert(buf_[0] == 't');
        }

        if (buf_pos_ < 4) {
            written = in.readRawData(buf_ + buf_pos_, 4 - buf_pos_);
            out.write(buf_ + buf_pos_, written);
            buf_pos_ += written;
            if (buf_pos_ < 4) {
                break;
            }
            buf_[buf_pos_] = '\0';
        }
        buf_[4] = '\0';
        QByteArray id = QByteArray::fromHex(buf_ + 1);

        if (buf_pos_ < 5) {
            written = in.readRawData(buf_ + buf_pos_, 1);
            out.write(buf_ + buf_pos_, written);
            buf_pos_ += written;
            if (buf_pos_ < 5) {
                break;
            }
            buf_sz_ = (buf_[buf_pos_ - 1] - '0');
            if (!(buf_sz_ <= 8 && buf_sz_ >= 0)) {
                in.readRawData(buf_ + buf_pos_, 100);
                qWarning() << buf_;
                buf_pos_ = 0;
                buf_sz_ = 0;
                break;
            }
        }

        if (buf_pos_ < 5 + buf_sz_ * 2 + 1) {
            written = in.readRawData(buf_ + buf_pos_, 5 + buf_sz_ * 2 + 1 - buf_pos_);
            out.write(buf_ + buf_pos_, written);
            buf_pos_ += written;
            if (buf_pos_ < 5 + buf_sz_ * 2 + 1) {
                break;
            }
            assert(buf_[buf_pos_ - 1] == '\r');
            buf_[buf_pos_] = '\0';
        }
        QByteArray data = QByteArray::fromHex(buf_ + 5);

        //qWarning() << "Id: " << id.toHex() << ", data: " << data.toHex();
        if (id.toHex() == "030a") {
            if (angles_ != pre_angles_ || distances_ != pre_distances_) {
                angles_ = pre_angles_;
                distances_ = pre_distances_;
                emit anglesChanged();
                emit distancesChanged();
            }

            pre_angles_.clear();
            pre_distances_.clear();
            lat_ = GetDouble(data);
            emit latChanged();
            //qWarning() << "Lat: " << lat_;
        } else if (id.toHex() == "030b") {
            lon_ = GetDouble(data);
            emit lonChanged();
            //qWarning() << "Lon: " << lon_;
        } else if (id.toHex() == "030c") {
            angle_ = GetDouble(data) * 57.295779513082321;
            emit angleChanged();
            //qWarning() << "Angle: " << angle_;
        } else if (id.toHex() == "030d") {
            speed_ = GetDouble(data);
            emit speedChanged();
            //qWarning() << "Speed: " << speed_;
        } else if (id.toHex() == "030e") {
            double angle = GetDouble(data) * 57.295779513082321;
            //qWarning() << "Lidar angle: " << angle;
            pre_angles_.append(angle - angle_);
            lid_angle_ = GetDouble(data);
        } else if (id.toHex() == "030f") {
            double dist = GetDouble(data);
            //qWarning() << "Distance: " << dist;
            pre_distances_.append(dist);
            cloudx_ = lon_ * 10000000. / 53. + dist * std::sin(lid_angle_);
            cloudy_ = lat_ * 500000. / 11. + dist * std::cos(lid_angle_);
            emit cloudyChanged();
        } else if (id.toHex() == "0123") {
            mark_ = data.toHex();
            emit markChanged();
        } else {
            qWarning() << "WTF?? " << "Id: " << id.toHex() << ", data: " << data;
        }
        buf_pos_ = 0;
        buf_sz_ = 0;
    }
}

void Car::updateParams() {
    if (left_) {
        auto bytes = buildFrame("30C", -5. / 180. * PI);
        tcpSocket->write(bytes.data(), bytes.size());
    } else if (right_) {
        auto bytes = buildFrame("30C", 5. / 180. * PI);
        tcpSocket->write(bytes.data(), bytes.size());
    } else {
        auto bytes = buildFrame("30C", 0);
        tcpSocket->write(bytes.data(), bytes.size());
    }

    if (up_) {
        auto bytes = buildFrame("30D", 0.5);
        tcpSocket->write(bytes.data(), bytes.size());
    } else if (down_) {
        auto bytes = buildFrame("30D", -0.5);
        tcpSocket->write(bytes.data(), bytes.size());
    } else {
        auto bytes = buildFrame("30D", 0);
        tcpSocket->write(bytes.data(), bytes.size());
    }
    tcpSocket->flush();
}

void Car::logError(QAbstractSocket::SocketError socketError) {
    qWarning() << tcpSocket->errorString();
}
