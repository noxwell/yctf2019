import QtQuick 2.12
import QtQuick.Window 2.12
import QtCharts 2.3
import my.car 1.0

Window {
    visible: true
    width: 700
    height: 1000
    title: qsTr("Remote control")

    Car {
        id: car
        onAnglesChanged: {
            series.clear();
            for (var i = 0; i <= Math.min(angles.length, distances.length); i++) {
                series.append(angles[i] + (angles[i] < 0. ? 360. : 0), distances[i]);
                //series.append(angles[i], distances[i]);
            }
            pose.clear();
            pose.append(car.lon * 10000000. / 53., car.lat * 500000. / 11.);
        }
        onCloudyChanged: {
            cloud.append(cloudx, cloudy);
            if (cloud.count > 1000) {
                cloud.clear();
            }
        }
    }

    property bool left: false
    property bool right: false
    property bool up: false
    property bool down: false

    Column {
        anchors.centerIn: parent
        Text {
            id: latText
            text: 'Lat: ' + car.lat.toString()
        }
        Text {
            id: lonText
            text: 'Lon: ' + car.lon.toString()
        }
        Text {
            id: speedText
            text: 'Speed: ' + car.speed.toString()
        }
        Text {
            id: angleText
            text: 'Angle: ' + car.angle.toString()//(car.angle / Math.PI * 180.0).toString()
        }
        Text {
            id: markText
            text: 'Mark: ' + car.mark
        }
        Text {
            id: keyText
            text: 'Key: ' + (car.left ? "left " : "") + (car.right ? "right " : "") + (car.up ? "up " : "") + (car.down ? "down " : "")
        }
        PolarChartView {
            antialiasing: true
            width: 400
            height: 400

            ValueAxis {
                id: axisAngular
                min: 0
                max: 360
                tickCount: 9
            }

            ValueAxis {
                id: axisRadial
                min: 0
                max: 5
            }

            LineSeries {
                id: series
                //markerSize: 5
                axisAngular: axisAngular
                axisRadial: axisRadial
            }
        }
        ChartView {
            width: 700
            height: 400
            ValueAxis {
                id: axisX
                min: 7158030.323812632
                max: 7158615.714648842
                tickCount: 9
            }

            ValueAxis {
                id: axisY
                min: 2506618.623601086
                max: 2506654.2448332654
            }
            ScatterSeries {
                id: cloud
                axisX: axisX
                axisY: axisY
                markerSize: 5
            }
            ScatterSeries {
                id: pose
                axisX: axisX
                axisY: axisY
                markerSize: 5
            }
        }
    }

    Item {
        anchors.fill: parent
        focus: true

        Keys.onPressed: {
            if (event.key === Qt.Key_Left) {
                car.left = true;
                event.accepted = true;
            }
            if (event.key === Qt.Key_Right) {
                car.right = true;
                event.accepted = true;
            }
            if (event.key === Qt.Key_Up) {
                car.up = true;
                event.accepted = true;
            }
            if (event.key === Qt.Key_Down) {
                car.down = true;
                event.accepted = true;
            }
        }
        Keys.onReleased: {
            if (event.key === Qt.Key_Left) {
                car.left = false;
                event.accepted = true;
            }
            if (event.key === Qt.Key_Right) {
                car.right = false;
                event.accepted = true;
            }
            if (event.key === Qt.Key_Up) {
                car.up = false;
                event.accepted = true;
            }
            if (event.key === Qt.Key_Down) {
                car.down = false;
                event.accepted = true;
            }
        }
    }
}
