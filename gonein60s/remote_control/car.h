#ifndef CAR_H
#define CAR_H

#include <QTcpSocket>
#include <QDataStream>
#include <QList>
#include <QObject>
#include <QFile>

class Car : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double lat READ lat NOTIFY latChanged)
    Q_PROPERTY(double lon READ lon NOTIFY lonChanged)
    Q_PROPERTY(double speed READ speed NOTIFY speedChanged)
    Q_PROPERTY(double angle READ angle NOTIFY angleChanged)
    Q_PROPERTY(QString mark READ mark NOTIFY markChanged)

    Q_PROPERTY(QList<qreal> angles READ angles NOTIFY anglesChanged)
    Q_PROPERTY(QList<qreal> distances READ distances NOTIFY distancesChanged)

    Q_PROPERTY(double cloudx READ cloudx NOTIFY cloudxChanged)
    Q_PROPERTY(double cloudy READ cloudy NOTIFY cloudyChanged)

    Q_PROPERTY(double left READ left WRITE setLeft NOTIFY leftChanged)
    Q_PROPERTY(double right READ right WRITE setRight NOTIFY rightChanged)
    Q_PROPERTY(double up READ up WRITE setUp NOTIFY upChanged)
    Q_PROPERTY(double down READ down WRITE setDown NOTIFY downChanged)

public:
    explicit Car(QObject *parent = nullptr);

    double lat();
    double lon();
    double speed();
    //void setSpeed(double lat);
    double angle();
    //void setAngle(double lat);
    QString mark();
    double cloudx();
    double cloudy();

    QList<qreal> angles();
    QList<qreal> distances();

    bool left();
    void setLeft(bool left);
    bool right();
    void setRight(bool left);
    bool up();
    void setUp(bool up);
    bool down();
    void setDown(bool down);

signals:
    void latChanged();
    void lonChanged();
    void speedChanged();
    void angleChanged();
    void markChanged();
    void cloudxChanged();
    void cloudyChanged();
    void anglesChanged();
    void distancesChanged();

    void leftChanged();
    void rightChanged();
    void upChanged();
    void downChanged();

private slots:
    void readData();
    void logError(QAbstractSocket::SocketError socketError);
    void updateParams();

private:
    double lat_ = 0.0;
    double lon_ = 1.0;
    double speed_ = 2.0;
    double angle_ = 3.0;
    QString mark_;
    double lid_angle_;
    double cloudx_;
    double cloudy_;

    QList<qreal> pre_angles_;
    QList<qreal> pre_distances_;
    QList<qreal> angles_;
    QList<qreal> distances_;

    bool left_ = false;
    bool right_ = false;
    bool up_ = false;
    bool down_ = false;

    QTcpSocket *tcpSocket = nullptr;
    QDataStream in;
    QFile out;
    char buf_[100];
    int buf_pos_ = 0;
    int buf_sz_ = 0;
};

#endif // CAR_H
